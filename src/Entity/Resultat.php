<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\ResultatRepository;

/**
 * @ORM\Entity(repositoryClass=ResultatRepository::class)
 */
class Resultat
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Election::class, inversedBy="resultats")
     */
    private $election;

    /**
     * @ORM\Column(type="json")
     */
    private $elected = [];

    /**
     * @ORM\Column(type="json")
     */
    private $ranking = [];

    // Getters and setters...

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getElection(): ?Election
    {
        return $this->election;
    }

    public function setElection(?Election $election): self
    {
        $this->election = $election;

        return $this;
    }

    public function getElected(): ?array
    {
        return $this->elected;
    }

    public function setElected(array $elected): self
    {
        $this->elected = $elected;

        return $this;
    }

    public function getRanking(): ?array
    {
        return $this->ranking;
    }

    public function setRanking(array $ranking): self
    {
        $this->ranking = $ranking;

        return $this;
    }
}