<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\BulletinRepository;

/**
 * @ORM\Entity(repositoryClass=BulletinRepository::class)
 */
class Bulletin
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="json")
     */
    private $classement = [];

    /**
     * @ORM\ManyToOne(targetEntity=Election::class, inversedBy="bulletins")
     * @ORM\JoinColumn(nullable=false)
     */
    private $election;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClassement(): ?array
    {
        return $this->classement;
    }

    public function setClassement(array $classement): self
    {
        $this->classement = $classement;

        return $this;
    }

    public function getElection(): ?Election
    {
        return $this->election;
    }

    public function setElection(?Election $election): self
    {
        $this->election = $election;

        return $this;
    }
}