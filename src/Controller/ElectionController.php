<?php

namespace App\Controller;

use App\Entity\Election;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ElectionController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/", name="index", methods={"GET"})
     */
    public function index(): Response
    {
        $elections = $this->entityManager->getRepository(Election::class)->findAll();

        return $this->json($elections, 200, [], ['groups' => 'election']);
    }

    #[Route('/election/create', name: 'create_new', methods: ['POST'])]
    public function new(Request $request): Response
    {
        // Récupérez les données de la requête
        $data = json_decode($request->getContent(), true);

        // Validez les données - Exemple minimal de validation
        if (!isset($data['titre']) || !isset($data['nombreDePlaces'])) {
            return $this->json(['error' => 'Missing required data'], 400);
        }

        // Créez une nouvelle instance d'Election et assignez les données
        $election = new Election();
        $election->setTitre($data['titre']);
        $election->setNombreDePlaces($data['nombreDePlaces']);

        // Persistez l'entité Election avec Doctrine
        $this->entityManager->persist($election);
        $this->entityManager->flush();

        // Retournez la nouvelle entité Election créée en réponse
        return $this->json($election, 201, [], ['groups' => 'election']);
    }

    /**
     * @Route("/{id}", name="show", methods={"GET"})
     */
    public function show(Election $election): Response
    {
        return $this->json($election, 200, [], ['groups' => 'election']);
    }

    /**
     * @Route("/{id}", name="delete", methods={"DELETE"})
     */
    public function delete(Election $election): Response
    {
        $this->entityManager->remove($election);
        $this->entityManager->flush();

        return $this->json(null, 204);
    }
}
