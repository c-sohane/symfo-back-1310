<?php

namespace App\Controller;

use App\Entity\Resultat;
use App\Entity\Election;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/resultats", name="resultat_")
 */
class ResultatController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/", name="index", methods={"GET"})
     */
    public function index(): Response
    {
        $resultats = $this->entityManager->getRepository(Resultat::class)->findAll();

        return $this->json($resultats, 200, [], ['groups' => 'resultat']);
    }

    /**
     * @Route("/", name="new", methods={"POST"})
     */
    public function new(Request $request): Response
    {
        $data = json_decode($request->getContent(), true);

        $electionId = $data['election'];
        $election = $this->entityManager->getRepository(Election::class)->find($electionId);

        if (!$election) {
            return $this->json(['error' => 'Election not found'], 404);
        }

        $resultat = new Resultat();
        $resultat->setElection($election);
        $resultat->setElected($data['elected']);
        $resultat->setRanking($data['ranking']);

        $this->entityManager->persist($resultat);
        $this->entityManager->flush();

        return $this->json($resultat, 201, [], ['groups' => 'resultat']);
    }

    /**
     * @Route("/{id}", name="show", methods={"GET"})
     */
    public function show(Resultat $resultat): Response
    {
        return $this->json($resultat, 200, [], ['groups' => 'resultat']);
    }
}
