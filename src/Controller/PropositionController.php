<?php

namespace App\Controller;

use App\Entity\Proposition;
use App\Entity\Election;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/propositions', name: 'proposition_')]
class PropositionController extends AbstractController
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    #[Route('/', name: 'index', methods: ['GET'])]
    public function index(): Response
    {
        $propositions = $this->entityManager->getRepository(Proposition::class)->findAll();
        dump($propositions); // Vérifiez ici si des données sont correctement récupérées
    
        return $this->json($propositions, 200, [], ['groups' => 'proposition']);
    }

    #[Route('/', name: 'create', methods: ['POST'])]
    public function create(Request $request): Response
    {
        $data = json_decode($request->getContent(), true);

        $electionId = $data['election'] ?? null;

        $election = $this->entityManager->getRepository(Election::class)->find($electionId);

        if (!$election) {
            return $this->json(['error' => 'Election not found'], 404);
        }

        $proposition = new Proposition();
        $proposition->setName($data['name']);
        $proposition->setElection($election);

        $this->entityManager->persist($proposition);
        $this->entityManager->flush();

        return $this->json($proposition, 201, [], ['groups' => 'proposition']);
    }

    #[Route('/{id}', name: 'show', methods: ['GET'])]
    public function show(Proposition $proposition): Response
    {
        return $this->json($proposition, 200, [], ['groups' => 'proposition']);
    }
}

