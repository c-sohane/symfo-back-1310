<?php

namespace App\Controller;

use App\Entity\Bulletin;
use App\Entity\Election;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/election/create', name: 'create_new', methods: ['POST'])]
class BulletinController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/", name="index", methods={"GET"})
     */
    public function index(): Response
    {
        $bulletins = $this->entityManager->getRepository(Bulletin::class)->findAll();

        return $this->json($bulletins, 200, [], ['groups' => 'bulletin']);
    }

    /**
     * @Route("/", name="new", methods={"POST"})
     */
    public function new(Request $request): Response
    {
        $data = json_decode($request->getContent(), true);

        $electionId = $data['election'];
        $election = $this->entityManager->getRepository(Election::class)->find($electionId);

        if (!$election) {
            return $this->json(['error' => 'Election not found'], 404);
        }

        $bulletin = new Bulletin();
        $bulletin->setClassement($data['classement']);
        $bulletin->setElection($election);

        $this->entityManager->persist($bulletin);
        $this->entityManager->flush();

        return $this->json($bulletin, 201, [], ['groups' => 'bulletin']);
    }

    /**
     * @Route("/{id}", name="show", methods={"GET"})
     */
    public function show(Bulletin $bulletin): Response
    {
        return $this->json($bulletin, 200, [], ['groups' => 'bulletin']);
    }
}
